from django.db import models


class Menu(models.Model):
    TYPES = (
        ('main', 'Главное меню'),
        ('bottom', 'Нижнее меню'),
    )

    menu_type = models.CharField('тип', max_length=20, choices=TYPES, unique=True)

    class Meta:
        verbose_name = 'меню'
        verbose_name_plural = 'меню'
        ordering = ['-id']

    def __str__(self):
        return self.get_menu_type_display()


class MenuItem(models.Model):
    title = models.CharField('заголовок', max_length=255)
    path = models.CharField('путь', max_length=255)

    menu = models.ForeignKey(
        Menu, on_delete=models.CASCADE,
        verbose_name=Menu._meta.verbose_name,
        related_name='item_set')
    parent = models.ForeignKey(
        'self', on_delete=models.SET_NULL,
        null=True, blank=True,
        verbose_name='родительский пункт',
        related_name='item_set')
    order = models.PositiveSmallIntegerField('порядок', default=0)

    class Meta:
        verbose_name = 'пункт'
        verbose_name_plural = 'пункты меню'
        ordering = ['order']

    def __str__(self):
        return self.title
