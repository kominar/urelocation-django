# from adminsortable2.admin import SortableInlineAdminMixin, SortableAdminMixin
from django.contrib import admin
from django import forms
from . import models


@admin.register(models.Menu)
class MenuAdmin(admin.ModelAdmin):
    list_display = ('menu_type',)
    save_on_top = True

    class MenuItemInline(admin.TabularInline):
        model = models.MenuItem

        class Form(forms.ModelForm):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                if self.instance and self.instance.pk:
                    self.fields['parent'].queryset = self.instance.menu.item_set.filter(parent_id__isnull=True)

            def clean(self):
                data = self.cleaned_data
                if data['parent'] and data['parent'].menu_id != data['menu'].id:
                    self.add_error('parent', 'Выбран пункт из другого меню')

        form = Form

    inlines = [MenuItemInline]


@admin.register(models.MenuItem)
class MenuItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'parent', 'menu', 'path')
    list_filter = ('menu', 'parent')
    list_select_related = ('menu', 'parent')
