jQuery(function($){
  $(document).ready(function() {
    
    $('.menu-selector').on('click', function() {
      $('.header').toggleClass('opened');
    });

    $('body').on('click', '.has-child:not(.menu-item-active) > a', function(e) {
      if ($(window).width() <= '1070'){
        e.preventDefault();
        $(this).parent('*').toggleClass('menu-item-active');
      }
    });

    $(window).scroll(function() {
        var scroll = $(window).scrollTop()
        if ( $('.upButton').length ) {
          if (scroll > 800) {
            $('.upButton, .bttn--sticky').addClass('active');
          } else {
            $('.upButton, .bttn--sticky').removeClass('active');
          }
        }

        if ( $('.logoList__body').length ) {
          $('.logoList__body').css({'margin-left': '-' + scroll + 'px'});
        }        
    });
  
    $('body').on('click', '.upButton', function(e) {
      e.preventDefault();
      $('html, body').stop().animate({
          scrollTop: ($('body').offset().top)
      }, 1000);
      return false;
    });
  
    if ($().inputmask) {
      $("[name=phone], input[type=tel]").inputmask("+7 (999) 999-99-99");
      $('body').on("focus", "[name=phone], input[type=tel]", function() {
        $(this).inputmask("+7 (999) 999-99-99");
      });
    }

    /* Открытые вопроса в FAQ */
    $(".faqItem").each(function() {
      if ( $(this).hasClass('active') ) {
        $(this).find('.faqItem__answer').slideToggle(400);
      }
    });
    $('.faqItem__ask').on('click', function() {
      $(this).parent('.faqItem').toggleClass('active');
      $(this).parent('.faqItem').find('.faqItem__answer').slideToggle(400);
    });

    if ($().owlCarousel) {
      $('.vacancySlider').owlCarousel({
          margin: 20,
          nav: true,
          items: 4,
          loop: false,
          autoHeight:true,
          dots: false,
          stagePadding: 0,
          responsive:{
            0:{
                items: 1,
                stagePadding: 20,
            },
            471:{
                items: 2,
            },
            771:{
                items: 3,
            },
            1071:{
                items: 4,
            },
        }          
      });

      $('.reviewSlider').owlCarousel({
          margin: 0,
          nav: true,
          items: 1,
          loop: false,
          autoHeight:true,
          dots: false,
          stagePadding: 0,
      });
    }

    /* Открытые модального окна */
    $('.js-gotomodal').on('click', function() {
      var target = $(this).data('target');
      $('#' + target).fadeIn(400);
      $('.modal-overlay').show();
      $('body').addClass('modaled');
    });

    /* Закрытые модального окна */
    $('.modal__close').on('click', function() {
      $('.modal').fadeOut(400);
      $('.modal-overlay').hide();
      $('body').removeClass('modaled');
    });
    $('.modal-overlay').on('click', function() {
        $(this).hide();
        $('.modal').fadeOut(400);
        $('body').removeClass('modaled');
    });
  });
});
