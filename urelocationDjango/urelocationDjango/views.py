from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    data = {"title": "Urelocation", "body_class": "home"}
    return render(request, 'homepage.html', context=data)


def thanks(request):
    data = {"title": "Thank you | Urelocation"}
    return render(request, 'thanks.html', context=data)


def testimonials(request):
    data = {"title": "Testimonials | Urelocation"}
    return render(request, 'testimonials.html', context=data)


def scoring(request):
    data = {"title": "Global Talent Visa | Urelocation"}
    return render(request, 'scoring.html', context=data)


def gtv(request):
    data = {"title": "Global Talent Visa | Urelocation"}
    return render(request, 'gtv.html', context=data)


def contacts(request):
    data = {"title": "Contacts | Urelocation"}
    return render(request, 'contacts.html', context=data)