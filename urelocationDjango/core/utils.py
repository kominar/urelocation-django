import string
import random
from datetime import date
from threading import Thread

from django.utils.html import format_html_join
from django.core.mail import EmailMultiAlternatives

chars = string.ascii_lowercase + string.digits


def randstring(n):
    return ''.join(random.choice(chars) for _ in range(n))


def upload_path(instance, filename):
    ext = filename.split('.')[-1]
    name = randstring(10)
    model_name = instance._meta.model_name
    now = date.today()
    return (
        '%s/%s/%s/%s/%s.%s' %
        (model_name, now.year, now.month, now.day, name, ext))


def to_html(text):
    return format_html_join(
        '', '<p>{}</p>',
        ((l,) for l in text.splitlines())
    )


def _send_mail(subject, message, recipient_list, attachments=None):
    if isinstance(recipient_list, str):
        recipient_list = [recipient_list]

    mail = EmailMultiAlternatives(
        subject, message,
        to=recipient_list,
        attachments=attachments
    )
    mail.send(fail_silently=False)


def send_mail(*args, **kwargs):
    thread = Thread(target=_send_mail, args=args, kwargs=kwargs)
    thread.start()


def replace_variables(s):
    return s.replace('%year%', str(date.today().year))
