import os
from string import Formatter

from django.db import models
from django.core.cache import cache

class StaticBlockManager(models.Manager):
    CACHE_KEY = 'static_blocks'

    def as_dict(self):
        dct = cache.get(self.CACHE_KEY)
        if dct is None:
            dct = {b.key: b.get_content() for b in self.all()}
            cache.set(self.CACHE_KEY, dct)
        return dct

    def clear_cache(self):
        cache.delete(self.CACHE_KEY)
        

class StaticBlock(models.Model):
    name = models.CharField('название', max_length=100)
    content = models.TextField('контент', blank=True)
    key = models.SlugField('ключ', unique=True)

    objects = StaticBlockManager()

    class Meta:
        verbose_name = 'блок'
        verbose_name_plural = 'статичные блоки'
        ordering = ['-id']

    def __str__(self):
        return self.name

    def clean(self):
        self.content = self.content.strip()
        # self.editor_content = self.editor_content.strip()

    def get_content(self):
        return self.content or self.editor_content


class SiteConfigManager(models.Manager):
    CACHE_KEY = 'siteconfig'

    def get_config(self):
        config = cache.get(self.CACHE_KEY)
        if config is None:
            config = self.get_or_create(id=1)[0]
            cache.set(self.CACHE_KEY, config)
        return config

    def clear_cache(self):
        cache.delete(self.CACHE_KEY)


class SiteConfig(models.Model):
    meta_title_suffix = models.CharField(
        'префикс мета-заголовка', max_length=255,
        default=' - Urelocation', blank=True
    )
    meta_description_suffix = models.CharField(
        'префикс мета-описания', max_length=255,
        default=' - Urelocation', blank=True
    )

    robots_txt = models.TextField('контент robots.txt', blank=True)

    objects = SiteConfigManager()

    class Meta:
        verbose_name = 'настройки сайта'
        verbose_name_plural = 'настройки сайта'

    def __str__(self):
        return 'настройки'

    def get_meta_templates(self, obj, context):
        view = context.get('view')
        if view is None:
            return

        template_name = getattr(view, 'meta_template_name', None) or context.get(
            'meta_template_name')

        if template_name:
            try:
                return (
                    getattr(self, 'meta_title_template_' + template_name),
                    getattr(self, 'meta_description_template_' + template_name)
                )
            except AttributeError:
                pass