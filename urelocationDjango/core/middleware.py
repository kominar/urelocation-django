from django.utils.functional import SimpleLazyObject
from django.conf import settings

from core.models import SiteConfig

class CoreMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.siteconfig = SimpleLazyObject(SiteConfig.objects.get_config)
        response = self.get_response(request)

        if 'utm_source' in request.GET:
            response.set_cookie(
                'utmsource',
                request.GET['utm_source'],
                max_age=10**10,
                domain=settings.BASE_DOMAIN
            )

        return response