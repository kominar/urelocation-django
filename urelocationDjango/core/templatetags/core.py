from django.template import Library, Node
from django.utils.html import mark_safe

from core.models import StaticBlock
from core import utils

register = Library()

@register.tag
def staticblock(parser, token):
    nodelist = parser.parse(('endstaticblock',))
    parser.delete_first_token()
    _, key = token.split_contents()
    return StaticBlockNode(nodelist, key=key[1:-1])


class StaticBlockNode(Node):
    def __init__(self, nodelist, key):
        self.nodelist = nodelist
        self.key = key

    def render(self, context):
        if '_static_blocks' not in context:
            context['_static_blocks'] = StaticBlock.objects.as_dict()

        if self.key not in context['_static_blocks']:
            block = StaticBlock.objects.create(
                name=self.key,
                key=self.key,
                content=self.nodelist.render(context)
            )
            context['_static_blocks'][self.key] = block.get_content()

        return mark_safe(utils.replace_variables(
            context['_static_blocks'][self.key]
        ))
