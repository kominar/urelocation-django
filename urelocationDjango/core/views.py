from django.http import HttpResponse


def robots_txt(request):
    return HttpResponse(request.siteconfig.robots_txt, content_type='text/plain')
