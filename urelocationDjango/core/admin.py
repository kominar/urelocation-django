import csv
import io
from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin
from django.http import HttpResponse
from . import models


class CodeMirrorMixin:
    code_fields = []

    class Media:
        css = {
            'all': (
                'admin/codemirror/codemirror.min.css',
            )
        }
        js = (
            'https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.48.4/codemirror.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.48.4/mode/xml/xml.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.48.4/mode/htmlmixed/htmlmixed.min.js',
            'admin/codemirror/init.js'
        )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self.code_fields:
            form.base_fields[field].widget.attrs['data-html'] = 1
        return form


class StaticBlockGroupFilter(admin.SimpleListFilter):
    title = 'Группа'
    parameter_name = 'group'

    def lookups(self, request, model_admin):
        keys = models.StaticBlock.objects.filter(key__contains=':').values_list(
            'key', flat=True)
        groups = set()

        for key in keys:
            try:
                group, _ = key.split(':', 1)
            except ValueError:
                continue

            groups.add(group)

        return [(g, g.title()) for g in sorted(groups)]

    def queryset(self, request, queryset):
        value = self.value()
        if value:
            return queryset.filter(key__startswith=value + ':')


@admin.register(models.StaticBlock)
class StaticBlockAdmin(CodeMirrorMixin, admin.ModelAdmin):
    list_display = ('name', 'key')
    search_fields = ('name', 'key', 'content')
    code_fields = ('content',)
    list_filter = (StaticBlockGroupFilter,)


@admin.register(models.SiteConfig)
class SiteConfigAdmin(admin.ModelAdmin):
    save_on_top = True
    fieldsets = (
        ('META', {
            'fields': (
                'meta_title_suffix',
                'meta_description_suffix',
            )
        }),
        ('Остальное', {
            'fields': (
                'robots_txt',
            )
        })
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields['meta_title_suffix'].strip = False
        form.base_fields['meta_description_suffix'].strip = False
        return form
